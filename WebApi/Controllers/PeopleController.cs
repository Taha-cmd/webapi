﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.DAL;
using WebApi.Entities;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly IAsyncPeopleRepository _repository;

        public PeopleController(IAsyncPeopleRepository repository)
        {
            _repository = repository;
        }

        // GET: People
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Person>>> GetPeople()
        {
            var people = await _repository.GetPeople();
            return people.ToList();
        }

        // GET: api/People/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> GetPerson(int id)
        {
            if (!await _repository.PersonExists(id))
            {
                return NotFound();
            }

            return await _repository.GetPerson(id);

        }

        // PUT: api/People/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPerson(int id, Person person)
        {
            if (id != person.PersonId)
            {
                return BadRequest();
            }
            if (!await _repository.PersonExists(id))
            {
                return NotFound();
            }
            await _repository.Update(id, person);



            return Created($"People/{person.PersonId}", person);

        }

        // POST: api/People
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Person>> PostPerson(Person person)
        {
            await _repository.Create(person);

            var people = await _repository.GetPeople();
            var result = people.OrderByDescending(p => p.PersonId).First();
            return Created($"People/{result.PersonId}", result);
        }

        // DELETE: api/People/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePerson(int id)
        {
            if (!await _repository.PersonExists(id))
            {
                return NotFound();
            }

            await _repository.Delete(id);

            return NoContent();
        }
    }
}
