FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env

WORKDIR /app
COPY . .
RUN dotnet restore
RUN dotnet publish --no-restore -c Release -o dist

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build-env /app/dist .

ENTRYPOINT ["dotnet", "WebApi.dll"]