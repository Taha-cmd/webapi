using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using WebApi.Controllers;
using WebApi.DAL;

namespace WebApi.Tests
{
    public class PeopleControllerTests
    {
        PeopleController _controller;

        [SetUp]
        public void Setup()
        {
            Mock<IAsyncPeopleRepository> mockObjectType = new();
            mockObjectType.Setup(m => m.PersonExists(It.IsAny<int>())).Returns(Task.FromResult(false));

            _controller = new PeopleController(mockObjectType.Object);
        }

        [Test]
        public async Task Test_Delete_Not_Existing_Person()
        {
            var result = await _controller.DeletePerson(5);

            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task Test_Update_Not_Existing_Person()
        {
            var result = await _controller.PutPerson(5, new Entities.Person() { PersonId = 5 });

            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task Test_Update_None_Matching_Id()
        {
            var result = await _controller.PutPerson(5, new Entities.Person() { PersonId = 7 }); Assert.IsInstanceOf<BadRequestResult>(result);


        }

        //[Test]
        //public void Fail()
        //{
        //    Assert.IsTrue(false);
        //}
    }
}