﻿using System;
using Microsoft.EntityFrameworkCore;
using WebApi.Entities;

namespace WebApi.DAL
{
    public class PersonContext : DbContext
    {
        public PersonContext(DbContextOptions<PersonContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
            .Property(f => f.PersonId)
            .ValueGeneratedOnAdd();
        }
        public DbSet<Person> People { get; set; }
    }
}
