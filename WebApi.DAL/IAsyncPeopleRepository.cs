﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Entities;

namespace WebApi.DAL
{
    public interface IAsyncPeopleRepository
    {
        public Task<IEnumerable<Person>> GetPeople();
        public Task<Person> GetPerson(int id);
        public Task Create(Person person);
        public Task Delete(int id);
        public Task Update(int id, Person person);
        public Task<bool> PersonExists(int id);

    }
}
