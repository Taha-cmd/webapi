﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Entities;

namespace WebApi.DAL
{
    public class AsyncPeopleRepository : IAsyncPeopleRepository
    {
        private readonly PersonContext _context;
        public AsyncPeopleRepository(PersonContext context)
        {
            _context = context;
        }
        public async Task Create(Person person)
        {
            await _context.People.AddAsync(person);
            _context.SaveChanges();
        }

        public async Task Delete(int id)
        {
            _context.People.Remove(await GetPerson(id));
            await _context.SaveChangesAsync();
        }

        public async Task<Person> GetPerson(int id)
        {
            return await _context.People.FindAsync(id);
        }

        public async Task<IEnumerable<Person>> GetPeople()
        {
            return await _context.People.ToListAsync();
        }

        public async Task<bool> PersonExists(int id)
        {
            return await _context.People.AnyAsync(e => e.PersonId == id);
        }

        public async Task Update(int id, Person person)
        {
            _context.Entry(person).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
